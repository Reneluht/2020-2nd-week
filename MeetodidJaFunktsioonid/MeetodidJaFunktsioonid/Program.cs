﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetodidJaFunktsioonid
{
    class Inimene
    {
        public string Nimi;
        public int Vanus;
    }
    class Program
    {
        static void Main(string[] args) // meetod Main (jaavas main väikese tähega)
        {
            // tegin eraldi väikese rakenduse, et funktsioone (ja meetodeid) arutada - proovi ka

            int koos = Liida(3, Liida(5, 8));
            Console.WriteLine(koos);
            Console.WriteLine(Faktoriaal(8));

            Console.WriteLine(Korruta(kaks: 17, yks: 4));

            int a = 3;
            int b = 10;
            Console.WriteLine($"a={a}, b={b}");
            Vaheta(ref a, ref b);
            Console.WriteLine($"a={a}, b={b}");

            string h = "Henn";
            string nts = "Ants";
            Vaheta(ref h, ref nts);

            Inimene i1 = new Inimene { Nimi = "Henn", Vanus = 64 };
            Inimene i2 = new Inimene { Nimi = "Ants", Vanus = 28 };

            Vaheta(ref i1, ref i2);

            List<int> arvud = new List<int>();

            Console.WriteLine(Add(new int[] { a, b }));
            Console.WriteLine(Add(3, 7));


        }

        // eriline funktsioon - rekursiivne
        static int Faktoriaal(int n)
        {
            return n < 2 ? 1 : Faktoriaal(n - 1) * n;
        }

        // üks hästi lihtne funktsioon
        static int Liida(int yks, int teine)
        {
            return yks + teine;
        }

        static int Liida(int yks, int kaks, int kolm)
        {
            return yks + kaks + kolm;
        }

        static int Korruta(int yks = 1, int kaks = 2)
        {
            return yks * kaks;
        }




        // siin on üks out parameetritega funktsioon
        static bool Arvuta(int yks, int teine, out int summa, out int korrutis)
        {
            summa = yks + teine;
            korrutis = yks * teine;
            return true;
        }

        static void Vaheta<T>(ref T vasak, ref T parem) // generic meetodi näide
        {
            //(vasak, parem) = (parem, vasak);

            T ajutine = vasak;
            vasak = parem;
            parem = ajutine;
        }

        //static int Add(int a) { return a; }
        //static int Add(int a, int b) { return a + b; }
        //static int Add(int a, int b, int c) { return a + b + c; }
        static int Add(params int[] arvud)
        {
            int s = 0; foreach (var x in arvud) s += x;
            return s;
        }



    }
}