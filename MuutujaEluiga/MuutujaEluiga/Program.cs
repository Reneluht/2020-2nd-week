﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuutujaEluiga
{
    class Program
    {
        static void Main(string[] args)
            // . - member call operation
            // [] - index call operation
            // ?. - conditional member call
            // ?? coalesce operation
            // 
        {
            Inimene i;
            i = new Inimene();

            Console.WriteLine(i);
            Console.WriteLine(i.Kaasa.Kaasa?.Nimi??"kaasat veel ei ole"); // ?. pöördu selle olematu asja nime poole

            var x = i?.Vanus;
            int? arv1 = 0;
            int? arv2 = 0;
            var tulemus = arv1 +arv2;

            Console.WriteLine(i.Kaasa?.Vanus);
            


        }
    }
    class Inimene // REFERENCE - definitsioonid
    {
        public static int InimesteArv = 0; // rakendub kui kasutatakse funktsiooni "Inimene();"

        public string Nimi;
        public int Vanus;
        public Inimene Kaasa;

        public int Sõrmi = 10;
        public int Varbad = 10;
        


        public override string ToString() => $"{Nimi} vanusega {Vanus}";
    }
    struct Miski //
    {
        public int InimesteArv;
        public string Nimetus;
        public int Väärtus;
    }
}
