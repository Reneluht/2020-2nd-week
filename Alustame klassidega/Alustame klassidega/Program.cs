﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alustame_klassidega
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] m1 = new int[5];
            //int[] m2 = new int[5] { 1, 2, 3, 4, 5 };
            //int[] m3 = new int[5] { 1, 2, 3, 4, 5 };
            //int[] m4 = { 1, 2, 3, 4, 5 };

            //List<int> l1 = new List<int>();
            //List<int> l2 = new List<int>() { 1, 2, 3, 4, 5 };
            //List<int> l3 = new List<int> { 1, 2, 3, 4, 5 };


            Inimene rene = new Inimene();
            rene.Eesnimi = "Rene Luht";
            rene.Vanus = 28;

            Console.WriteLine(rene);

            Inimene teine = new Inimene
            {
                Eesnimi = "Kaval",
                Perenimi = "Ants",
                Vanus = 27
            };

            Console.WriteLine(teine);
            //Console.WriteLine($"Meil on inimesi {_InimesteArv}");
        }
    }

    partial class Inimene
    {
        private static int InimesteArv = 0; // static välju on kamba peale 1
        //static string Tulevik = "Helge";
        //public static int InimesteArv => _InimesteArv();
        //public static string Tulevik => _Tulevik;

        public int Number = ++InimesteArv; //mittestaatilisi väljad on igal eksemplaril oma
        public string Eesnimi;
        public string Perenimi;
        public int Vanus;

        public string Nimi ()
        {
            return $"{Eesnimi} {Perenimi}";
        }


        public override string ToString() => $"{Number}. inimene {Nimi()} vanusega {Vanus}";
    }
}
