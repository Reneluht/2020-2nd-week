﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KlassiÜL
{
    class Inimene
    {
        public static void Main(string[] args)
        {
            Inimene rene = new Inimene();
            rene.Eesnimi = "Rene";
            rene.Perenimi = "Luht";
            rene.Isikukood = "39110290288";

            Console.WriteLine(rene.Nimi());
            Console.WriteLine(rene.Sugu() == 0 ? "naine" : "mees");
            Console.WriteLine(rene.Sünniaeg());
            Console.WriteLine(rene.Vanus());
        }

        public string Eesnimi;
        public string Perenimi;
        private string Isikukood;

        public int Vanus()
        {
            return (DateTime.Today - Isikukood[1, 2]);
        }

        public DateTime Sünniaeg()
        {

        }

        public int Sugu()
        {
            return Isikukood[0] % 2; 
        }


        public string Nimi()
        {
            return $"{Eesnimi} {Perenimi}";
        }
    }
}
