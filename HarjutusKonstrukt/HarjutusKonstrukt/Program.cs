﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarjutusKonstrukt
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            new Inimene("35503070211") { Eesnimi = "henn", Perenimi = "SARV" };
            new Inimene("45705210211") { Eesnimi = "MAris", Perenimi = "SARV" };
            new Inimene("61601260000") { Eesnimi = "grete", Perenimi = "ValdVee" };
            new Inimene("35503070211") { Eesnimi = "henn", Perenimi = "SARV" };
            */
            var i = Inimene.New("35503070211");  // isikukoodi ei ole - tehakse uus
            i.Eesnimi = "henn";
            i.Perenimi = "sarv";

            {
                if (Inimene.TeeInimene("35503070211", out Inimene uus))
                {
                    uus.Eesnimi = "Henn";
                    uus.Perenimi = "sarv";
                }
            }
            {
                if (Inimene.TeeInimene("45705050211", out Inimene uus))
                {
                    uus.Eesnimi = "Henn";
                    uus.Perenimi = "sarv";
                }
            }
            {
                if (Inimene.TeeInimene("35503070211", out Inimene uus))
                {
                    uus.Eesnimi = "Henn";
                    uus.Perenimi = "sarv";
                }
            }



            foreach (var x in Inimene.Inimesed) Console.WriteLine(x);
            Console.WriteLine(Inimene.InimesteArv);
        }
            enum Sugu { Naine, Mees }

        class Inimene
        {
            // staatilised asjad
            public static int InimesteArv { get; private set; } = 0; // inimeste loendur
            static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
            public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;      //        public Dictionary<string, Inimene>.ValueCollection Inimesed => _Inimesed.Values;


            // objekti asjad
            // väljad ja propertid
            public readonly int Nr = ++InimesteArv;
            public string Eesnimi { get => _Eesnimi; set => _Eesnimi = ToProper(value); }
            private string _Eesnimi;
            public string Perenimi { get => _Perenimi; set => _Perenimi = ToProper(value); }
            private string _Perenimi;
            public string Nimi => Eesnimi + " " + Perenimi;

            public string Isikukood { get; private set; }

            // konstruktorid
            private Inimene(string isikukood)
            {
                Isikukood = isikukood;
                if (!_Inimesed.ContainsKey(isikukood))
                    _Inimesed.Add(isikukood, this);
                // else puhul võiks mõelda, kas anda teada või kisada niisama või ignoreerida
                // aga see kontrollib, et dictionary ei läheks KAHTE sama võtmega
            }

            public static Inimene New(string isikukood)
            => _Inimesed.ContainsKey(isikukood)
                    ? null
                    : new Inimene(isikukood);

            public static bool TeeInimene(string isikukood, out Inimene inimene)
            {
                inimene = null;
                if (_Inimesed.ContainsKey(isikukood)) return false;
                else inimene = new Inimene(isikukood);
                return true;
            }


            // funktsioonid ja readonly propertid
            public Sugu Sugu => (Sugu)(Isikukood[0] % 2);

            public DateTime Sünniaeg =>
                DateTime.Parse(
                    (
                        Isikukood[0] < '3' ? "18" :
                        Isikukood[0] < '5' ? "19" :
                        Isikukood[0] < '7' ? "20" :
                        "21"
                    )
                    + Isikukood.Substring(1, 2) +
                    "/" + Isikukood.Substring(3, 2) +
                    "/" + Isikukood.Substring(5, 2)
                    );

            // meetodid
            static string ToProper(string s)
                => s == "" ? "" :
                (s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower());

            // overraidid
            public override string ToString() => $"{Nr}. {Sugu} {Nimi} sündinud {Sünniaeg:dd.MM.yyyy}";

        }
    }
}
