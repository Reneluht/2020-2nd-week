﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Spordipäev_vol2
{
    class Nimed
    {
        static string Küsi(string küsimus) // funktsioon
        {
            Console.WriteLine(küsimus + ": ");
            string vastus = Console.ReadLine();
            if (vastus == "") return "ta ei taha öelda";
            else return vastus;
        }

        static void Trüki(string nimi, string sisu) // see on meetod
        {
            Console.WriteLine($"{nimi}" + $"{sisu}");
        }
        static void Main(string[] args)
        {
            //Console.WriteLine("Kes sa oled: ");
            //string nimi = Console.ReadLine();

            //Console.WriteLine("Kus sa elad: ");
            //string nimi = Console.ReadLine();

            string nimi = Küsi("Kes sa oled");
            string aadress = Küsi("Kus sa elad");

            Trüki("tema nimi on: ", nimi);
            Trüki("tema aadress on: ", aadress);


            string fileName = @"..\..\spordipäeva.txt";
            var loetudRead = File.ReadAllLines(fileName);
            List<ProtokolliRida> protokoll = new List<ProtokolliRida>();

            for (int i = 1; i < loetudRead.Length; i++)
            {
                var tükid = loetudRead[i].Replace(", ", ",").Split(',');
                var rida = new ProtokolliRida
                {
                    Nimi = tükid[0],
                    Distants = int.Parse(tükid[1]),
                    Aeg = int.Parse(tükid[2]),
                };
                rida.Kiirus = rida.Aeg == 0 ? 0 : rida.Distants * 1.0 / rida.Aeg;
                protokoll.Add(rida);
            }
            foreach (var x in protokoll)
            {
                Console.WriteLine(x);
            }
        }

    }
    class ProtokolliRida
    {
        public string Nimi;
        public int Distants;
        public int Aeg;
        public double Kiirus;

        //Näide meetodist
        //public void TrükiTulemus()
        //{
        //    Console.WriteLine($"{Nimi} jooksis {Distants} m ajaga {aeg}");
        //}

        ////Näide funktsioonist
        //public double Kiirus()
        //{
        //    return Distants * 1.0 / Aeg;
        //}

        public override string ToString()
        {
            return $"{Nimi} jooksis {Distants} m kiirusega {Aeg}s";
        }
    }
}
