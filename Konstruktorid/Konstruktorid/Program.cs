﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konstruktorid
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene rene = new Inimene("39110290288")
            {
                Nimi = "Rene",
                Vanus = 28,
                nr = 1
            };
            Inimene Ats = new Inimene("42312020987")
            {
                Nimi = "Rene",
                Vanus = 99,
                nr = 2
            };
            Inimene Mats = new Inimene("34910190213")
            {
                Nimi = "Rene",
                Vanus = 70,
                nr = 3
            };
            Console.WriteLine(rene);
            Console.WriteLine(rene.Isikukood);
        }
    }
    class Inimene
    {
        public static int InimesteArv = 0; // staatiline ehk ühine väli
        //private static int InimesteArv { get; private set; } = 0;

        static List<Inimene> Inimesed = new List<Inimene>();
        

        public int nr = ++InimesteArv;
        public string Nimi;
        public int Vanus;

        public readonly string Isikukood;
        public Inimene(string Isikukood) : this()
        {
            Isikukood = Isikukood;
            Inimesed.Add(this);
        }

        public Inimene()
        {
            Isikukood = "ilmakoodita";
            Inimesed.Add(this);
        }

        public override string ToString() => $"{nr}. {Nimi} vanus: {Vanus} (kokku {InimesteArv} {(InimesteArv == 1 ? "inimene" : "inimest")})";
    }
}
